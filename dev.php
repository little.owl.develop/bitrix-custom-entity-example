<?php
/**
 * Эта страница использовалась только во время разработки для инициализации сущности
 * (таблица и стартовые данные)
 */

use Lib\Entities;

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

/**
 * Это использовалось для начальной инициализации, описание в исходниках этих методов
 * /local/php_interface/functions/dev_only.php
 */
// DevOnly\initEntitiesTables();
// DevOnly\seedCityFinance();

/**
 * Это пример ручного вызова пересчёта рейтинга
 */
// Entities\CityFinanceTable::recalcRatingPositions();

/**
 * Это простой пример добавления нового элемента, при этом рейтинг для всех элементов будет пересчитан
 */
// Entities\CityFinanceTable::add([
//     'CITY_NAME' => 'Rome',
//     'RESIDENTS_INCOME' => 50000,
//     'RESIDENTS_EXPENSES' => 18000,
//     'RESIDENTS_COUNT' => 1200,
// ]);