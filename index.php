<?php
/** @global CMain $APPLICATION */

use Bitrix\Main\Localization\Loc;
use Lib\Entities\CityFinanceTable;

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php';

Loc::loadMessages(__FILE__);

$APPLICATION->SetTitle(Loc::getMessage('index.TITLE'));
?>
<?php
/**
 * Эту таблицу можно (и нужно) оформить в виде компонента, но для текущего этапа демо
 * такой задачи не стояло, поэтому я принял решение упростить задачу,
 * чтобы легче было сконцентрироваться на главном (самой сущности).
 * В реальной разработке такое недопустимо
 */

$tableData = CityFinanceTable::getList([
    'order' => ['RESIDENTS_COUNT' => 'DESC'],
])->fetchAll();
?>
<table class="table table-bordered table-striped">
    <thead class="thead-dark">
        <tr>
            <th><?=Loc::getMessage('table.CITY_NAME')?></th>
            <th><?=Loc::getMessage('table.RESIDENTS_INCOME')?></th>
            <th><?=Loc::getMessage('table.RESIDENTS_EXPENSES')?></th>
            <th><?=Loc::getMessage('table.RESIDENTS_COUNT')?></th>
            <th><?=Loc::getMessage('table.RESIDENTS_COUNT_RATING_POS')?></th>
            <th><?=Loc::getMessage('table.RUNTIME.AVG_INCOME')?></th>
            <th><?=Loc::getMessage('table.AVG_INCOME_RATING_POS')?></th>
            <th><?=Loc::getMessage('table.RUNTIME.AVG_EXPENSES')?></th>
            <th><?=Loc::getMessage('table.AVG_EXPENSES_RATING_POS')?></th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach ($tableData as $row) {
        ?>
        <tr>
            <td><?=$row['CITY_NAME']?></td>
            <td><?=Lib\Theme\Helper::formatPrice($row['RESIDENTS_INCOME'])?></td>
            <td><?=Lib\Theme\Helper::formatPrice($row['RESIDENTS_EXPENSES'])?></td>
            <td><?=Lib\Theme\Helper::formatNumber($row['RESIDENTS_COUNT'])?></td>
            <td><?=Lib\Theme\Helper::formatNumber($row['RESIDENTS_COUNT_RATING_POS'])?></td>
            <td><?=Lib\Theme\Helper::formatPrice($row['RESIDENTS_INCOME'] / $row['RESIDENTS_COUNT'])?></td>
            <td><?=Lib\Theme\Helper::formatNumber($row['AVG_INCOME_RATING_POS'])?></td>
            <td><?=Lib\Theme\Helper::formatPrice($row['RESIDENTS_EXPENSES'] / $row['RESIDENTS_COUNT'])?></td>
            <td><?=Lib\Theme\Helper::formatNumber($row['AVG_EXPENSES_RATING_POS'])?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
<?php
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php';
?>