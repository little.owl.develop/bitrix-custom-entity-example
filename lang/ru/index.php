<?php
$MESS['index.TITLE'] = 'Пример таблицы с кастомными данными';

$MESS['table.CITY_NAME'] = 'Название';
$MESS['table.RESIDENTS_INCOME'] = 'Доходы общие';
$MESS['table.RESIDENTS_EXPENSES'] = 'Расходы общие';
$MESS['table.RESIDENTS_COUNT'] = 'Количество жителей';
$MESS['table.RESIDENTS_COUNT_RATING_POS'] = 'Место в рейтинге по количеству жителей';
$MESS['table.AVG_INCOME_RATING_POS'] = 'Место в рейтинге по средним доходам населения';
$MESS['table.AVG_EXPENSES_RATING_POS'] = 'Место по средним расходам населения';

$MESS['table.RUNTIME.AVG_INCOME'] = 'Средний доход';
$MESS['table.RUNTIME.AVG_EXPENSES'] = 'Средние расходы';