<?php
/** @global CMain $APPLICATION */

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

$assets = \Bitrix\Main\Page\Asset::getInstance();
$assets->addCss(SITE_TEMPLATE_PATH . '/css/lib/bootstrap/bootstrap.css');
$assets->addCss(SITE_TEMPLATE_PATH . '/css/main.css');
?>
<html lang="ru">
<head>
    <?php $APPLICATION->ShowHead(); ?>
    <title><?php $APPLICATION->ShowTitle(); ?></title>
</head>
<body>

<?php $APPLICATION->ShowPanel(); ?>
<div class="container">