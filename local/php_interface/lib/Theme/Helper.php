<?php


namespace Lib\Theme;


class Helper
{
    /**
     * Должно браться из отдельного класса с настройками, но для демо упрощаю
     * @var string
     */
    static private $priceFormat = '{currency}&nbsp;{value}';
    
    /**
     * Общий для темы формат числа
     * @param $number
     * @return string
     */
    public static function formatNumber($number)
    {
        $decimals = 2;
        if (intval($number) == $number) {
            $decimals = 0;
        }
        return number_format($number, $decimals, '.', ' ');
    }
    
    /**
     * Общий для темы формат цены
     * @param $value
     * @param string $currency
     * @return string|string[]|null
     */
    public static function formatPrice($value, $currency = '$')
    {
        $priceNumberFormatted = self::formatNumber($value);
        return preg_replace(
            ['/{value}/', '/{currency}/'],
            [$priceNumberFormatted, $currency],
            self::$priceFormat
        );
    }
}