<?php

namespace Lib\Entities;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\Event;

class CityFinanceTable extends Entity\DataManager
{
    /**
     * @return array
     * @throws \Exception
     */
    public static function getMap()
    {
        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            new Entity\StringField('CITY_NAME', ['required' => true]),
            new Entity\IntegerField('RESIDENTS_INCOME', ['required' => true]),
            new Entity\IntegerField('RESIDENTS_EXPENSES', ['required' => true]),
            new Entity\IntegerField('RESIDENTS_COUNT', ['required' => true]),
            
            // Rating fields
            new Entity\IntegerField('RESIDENTS_COUNT_RATING_POS', ['default_value' => -1]),
            new Entity\IntegerField('AVG_INCOME_RATING_POS', ['default_value' => -1]),
            new Entity\IntegerField('AVG_EXPENSES_RATING_POS', ['default_value' => -1]),
        ];
    }
    
    /**
     * Пересчёт полей рейтинга для всех элементов
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function recalcRatingPositions()
    {
        // Коллекция
        $entityCollection = self::getList()->fetchCollection();
        // Массив
        $entityArray = self::getList()->fetchAll();
        
        // Результирующий массив для быстрого поиска элемента по ключу, который будет содержать ID
        $resultArray = []; // keys are: id-{$ID}
        
        // Первый проход по массиву - сбор данных
        foreach ($entityArray as $item) {
            $id = $item['ID'] = intval($item['ID']);
            
            // prepare data
            $item['RESIDENTS_INCOME'] = intval($item['RESIDENTS_INCOME']);
            $item['RESIDENTS_EXPENSES'] = intval($item['RESIDENTS_EXPENSES']);
            $item['RESIDENTS_COUNT'] = intval($item['RESIDENTS_COUNT']);
            $item['RESIDENTS_COUNT_RATING_POS'] = intval($item['RESIDENTS_COUNT_RATING_POS']);
            $item['AVG_INCOME_RATING_POS'] = intval($item['AVG_INCOME_RATING_POS']);
            $item['AVG_EXPENSES_RATING_POS'] = intval($item['AVG_EXPENSES_RATING_POS']);
            
            // Runtime fields - вычисляем промежуточные значения для дальнейшего определения позиции
            
            // Средний доход населения
            $item['RUNTIME.AVG_INCOME'] = floatval($item['RESIDENTS_INCOME'] / $item['RESIDENTS_COUNT']);
            // Средние расходы населения
            $item['RUNTIME.AVG_EXPENSES'] = floatval($item['RESIDENTS_EXPENSES'] / $item['RESIDENTS_COUNT']);
            // Флаг изменения, пригодится далее
            $item['RUNTIME.CHANGED'] = false;
            
            $resultArray['id-' . $id] = $item;
        }
        
        // Сортировка по количеству населения
        $sortedByResidents = $resultArray;
        usort($sortedByResidents, function ($a, $b) {
            if ($a['RESIDENTS_COUNT'] == $b['RESIDENTS_COUNT']) {
                return 0;
            }
            // Чем больше значение, тем меньше номер
            return ($a['RESIDENTS_COUNT'] > $b['RESIDENTS_COUNT']) ? -1 : 1;
        });
        // Обновление рейтинга по заданному полю
        foreach ($sortedByResidents as $key => $item) {
            $oldVal = $resultArray['id-' . $item['ID']]['RESIDENTS_COUNT_RATING_POS'];
            $newVal = intval($key) + 1;
            
            if ($oldVal != $newVal) {
                $resultArray['id-' . $item['ID']]['RESIDENTS_COUNT_RATING_POS'] = $newVal;
                $resultArray['id-' . $item['ID']]['RUNTIME.CHANGED'] = true;
            }
        }
    
        // Сортировка по среднему доходу
        $sortedByAvgIncome = $resultArray;
        usort($sortedByAvgIncome, function ($a, $b) {
            if ($a['RUNTIME.AVG_INCOME'] == $b['RUNTIME.AVG_INCOME']) {
                return 0;
            }
            // Чем больше значение, тем меньше номер
            return ($a['RUNTIME.AVG_INCOME'] > $b['RUNTIME.AVG_INCOME']) ? -1 : 1;
        });
        // Обновление рейтинга по заданному полю
        foreach ($sortedByAvgIncome as $key => $item) {
            $oldVal = $resultArray['id-' . $item['ID']]['AVG_INCOME_RATING_POS'];
            $newVal = intval($key) + 1;
        
            if ($oldVal != $newVal) {
                $resultArray['id-' . $item['ID']]['AVG_INCOME_RATING_POS'] = $newVal;
                $resultArray['id-' . $item['ID']]['RUNTIME.CHANGED'] = true;
            }
        }
        
        // Сортировка по средним расходам
        $sortedByAvgExpenses = $resultArray;
        usort($sortedByAvgExpenses, function ($a, $b) {
            if ($a['RUNTIME.AVG_EXPENSES'] == $b['RUNTIME.AVG_EXPENSES']) {
                return 0;
            }
            // Чем меньше значение, тем меньше номер
            return ($a['RUNTIME.AVG_EXPENSES'] < $b['RUNTIME.AVG_EXPENSES']) ? -1 : 1;
        });
        // Обновление рейтинга по заданному полю
        foreach ($sortedByAvgExpenses as $key => $item) {
            $oldVal = $resultArray['id-' . $item['ID']]['AVG_EXPENSES_RATING_POS'];
            $newVal = intval($key) + 1;
        
            if ($oldVal != $newVal) {
                $resultArray['id-' . $item['ID']]['AVG_EXPENSES_RATING_POS'] = $newVal;
                $resultArray['id-' . $item['ID']]['RUNTIME.CHANGED'] = true;
            }
        }
        
        // Второй проход по массиву (теперь уже по коллекции) - запись данных
        foreach ($entityCollection as $collectionItem) {
            $id = $collectionItem->get('ID');
            
            // Элемент из собранного ранее массива по ID
            $itemFromArray = $resultArray['id-' . $id];
            
            // Если ничего не изменилось, пропускаем
            if (!$itemFromArray['RUNTIME.CHANGED']) continue;
            
            $collectionItem->set('RESIDENTS_COUNT_RATING_POS', $itemFromArray['RESIDENTS_COUNT_RATING_POS']);
            $collectionItem->set('AVG_INCOME_RATING_POS', $itemFromArray['AVG_INCOME_RATING_POS']);
            $collectionItem->set('AVG_EXPENSES_RATING_POS', $itemFromArray['AVG_EXPENSES_RATING_POS']);
        }
        
        $entityCollection->save(true); // with ignore events
    }
    
    /**
     * @param Event $event
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function onAfterUpdate(Event $event)
    {
        parent::onAfterUpdate($event);
        self::recalcRatingPositions();
    }
    
    /**
     * @param Event $event
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function onAfterAdd(Event $event)
    {
        parent::onAfterAdd($event);
        self::recalcRatingPositions();
    }
    
    /**
     * @param Event $event
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function onAfterDelete(Event $event)
    {
        parent::onAfterDelete($event);
        self::recalcRatingPositions();
    }
}