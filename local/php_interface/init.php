<?php

/**
 * Autoload Lib namespace classes
 * @throws \Bitrix\Main\LoaderException
 */
function libAutoload()
{
    
    $fLoadLibClasses = function ($dir, $rootNameSpace, $rootModuleDir)
    {
        $arClasses = [];
        
        $classDirIterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir),
            RecursiveIteratorIterator::SELF_FIRST
        );
        
        foreach ($classDirIterator as $filePath => $fileObj) {
            if (in_array($filePath, ['..', '.'])) {
                continue;
            }
            if (is_dir($filePath)) {
                continue;
            }
            
            $splitDir = explode(DIRECTORY_SEPARATOR, $filePath);
            $fileName = array_pop($splitDir);
            $dirName = array_pop($splitDir);
            
            $className = explode('.', $fileName);
            $className = array_shift($className);
            
            $arClasses[$rootNameSpace . '\\' .
            (!in_array($dirName, ['classes', 'lib']) ? ucfirst($dirName) . '\\' : '') .
            ucfirst($className)] = DIRECTORY_SEPARATOR . str_replace($rootModuleDir . DIRECTORY_SEPARATOR, '', $filePath);
        }
        
        return $arClasses;
    };
    
    // load classes
    $rootNameSpace = 'Lib';
    
    $allClassesDir = __DIR__ . DIRECTORY_SEPARATOR . 'lib';
    $classDirIterator = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($allClassesDir),
        RecursiveIteratorIterator::SELF_FIRST
    );
    
    foreach ($classDirIterator as $filePath => $fileObj) {
        if (in_array($fileObj->getFileName(), ['..', '.'])) {
            continue;
        }
        if (!is_dir($filePath)) {
            continue;
        }
        $classesDirPath = $filePath;
        \Bitrix\Main\Loader::registerAutoLoadClasses(
            null,
            $fLoadLibClasses(
                $classesDirPath,
                $rootNameSpace,
                realpath($_SERVER['DOCUMENT_ROOT'])
            )
        );
    }
}

libAutoload();

/*
 * Include functions
 */
if (file_exists(__DIR__ . '/functions/common.php')) {
    require __DIR__ . '/functions/common.php';
}

if (file_exists(__DIR__ . '/functions/dev_only.php')) {
    require __DIR__ . '/functions/dev_only.php';
}