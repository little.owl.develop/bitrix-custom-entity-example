<?php
/**
 * Only development and test functions
 */

namespace DevOnly;

use \Lib\Entities\CityFinanceTable;
use \Bitrix\Main\Application as App;
use \Bitrix\Main\Entity\Base as OrmBase;

/**
 * Инициализация таблиц сущностей. Обычно этот код является частью установщика модуля
 */
function initEntitiesTables()
{
    $ormArray = [
        '\Lib\Entities\CityFinanceTable'
    ];
    
    foreach ($ormArray as $ormName) {
        $connectionName = call_user_func("{$ormName}::getConnectionName");
        $connection = App::getConnection($connectionName);
        
        $tableName = OrmBase::getInstance($ormName)->getDBTableName();
        $tableExists = $connection->isTableExists($tableName);
        
        if (!$tableExists) {
            OrmBase::getInstance($ormName)->createDbTable();
        }
    }
}

/**
 * Простое заполнение таблицы конеретной сущности стартовыми значениями
 * @throws \Bitrix\Main\ArgumentException
 */
function seedCityFinance()
{
    // Стартовые значения. Можно было передавать аргументом, но для демо упростил до такого вида
    // CITY_NAME, RESIDENTS_INCOME, RESIDENTS_EXPENSES, RESIDENTS_COUNT
    $dataSeed = [
        ['Moscow', 15000, 3000, 900],
        ['London', 25000, 4000, 450],
        ['Paris', 10000, 1000, 550],
        ['Berlin', 9000, 1200, 120],
    ];
    
    // Коллекция
    $entityCollection = CityFinanceTable::getList()->fetchCollection();
    // Имя класса объекта для добавления в коллекцию
    $objClassName = CityFinanceTable::getObjectClass();
    
    foreach ($dataSeed as $row) {
        $newObj = new $objClassName;
        
        $j = -1;
        
        $newObj->set('CITY_NAME', $row[++$j]);
        $newObj->set('RESIDENTS_INCOME', $row[++$j]);
        $newObj->set('RESIDENTS_EXPENSES', $row[++$j]);
        $newObj->set('RESIDENTS_COUNT', $row[++$j]);
        
        $entityCollection[] = $newObj;
    }
    
    $entityCollection->save(true); // with ignore events
    
    // Вызов пересчёта полей, которые зависят от всех элементов в таблице
    CityFinanceTable::recalcRatingPositions();
}